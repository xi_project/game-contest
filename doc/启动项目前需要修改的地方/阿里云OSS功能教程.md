## 阿里云OSS模块启动教程
##1、开通阿里云OSS功能 [点击进入](https://oss.console.aliyun.com/overview) <br />
3.1、创建一个Bucket
![点击进入](./img/OSS.png) <br />
3.2、名字随便取，存储类型选低频访问，读写权限选公共读，不然读取不到
![点击进入](./img/OSS1.png) <br />

keyid：AccessKey账号<br />
keysecret：AccessKey密码<br />
endpoint：地域节点<br />
![点击进入](./img/OSS2.png) <br />
templateCode：Bucket域名<br />
![点击进入](./img/OSS3.png) <br />
