## 阿里云SMS模块启动教程
##1、如果您没有阿里云账号请先注册一个阿里云账号 <br />
##2、获取AccessKey密钥 <br />
![阿里云获取密钥](./img/阿里云获取密钥.png) <br />
2.1、点击开始使用子用户就可以了（当然如果您想使用AccessKey账户也可以） <br />
![阿里云获取密钥](./img/阿里云获取密钥1.png) <br />
2.2、点击创建用户
![阿里云获取密钥](./img/阿里云获取密钥2.png)
2.3、输入信息。<br /> 
**请一定要勾选编程访问** <br />
**请保存自己的AccessKey账户，因为阿里云功能都需要用到它**
![阿里云获取密钥](./img/阿里云获取密钥3.png)
##3、开通阿里云MSM功能 [点击进入](https://dysms.console.aliyun.com/dysms.htm) <br />
3.1、添加一个签名，添加一个模板（需要审核，用处就写公司需要用，需要先学习如何使用） <br />
短信费用0.045元/条，需要往阿里云充值一块钱<br />
![点击进入](./img/MSM.png) <br />
keyid：AccessKey账号<br />
keysecret：AccessKey密码<br />
signName：签名名称<br />
![点击进入](./img/MSM1.png) <br />
templateCode：模板CODE<br />
![点击进入](./img/MSM2.png) <br />
