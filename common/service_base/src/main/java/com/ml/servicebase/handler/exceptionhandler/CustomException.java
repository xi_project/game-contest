package com.ml.servicebase.handler.exceptionhandler;

import com.ml.commonutils.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/23 15:54
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CustomException extends RuntimeException{

    // 状态码
    private Integer code;
    // 异常信息
    private String msg;

    public CustomException(String msg) {
        this.code = ResultCode.ERROR;
        this.msg = msg;
    }

}
