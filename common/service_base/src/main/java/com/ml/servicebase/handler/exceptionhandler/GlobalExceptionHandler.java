package com.ml.servicebase.handler.exceptionhandler;

import com.ml.commonutils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一异常处理
 *
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/23 15:53
 */
@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    // 指定出现什么异常执行这个方法
    @ExceptionHandler(Exception.class)
    @ResponseBody // 为了返回数据
    public Result error(Exception e) {
        log.error(e.getMessage()); // 打印error信息
        e.printStackTrace();
        return Result.error().message("执行了全局异常处理");
    }

    // 自定义异常处理
    @ExceptionHandler(CustomException.class) // 如果抛出 CustomException异常 ， 就会执行这段代码
    @ResponseBody // 为了返回数据
    public Result error(CustomException e) {
        log.error(e.getMessage());
        e.printStackTrace();
        return Result.error().code(e.getCode()).message(e.getMsg());
    }

}
