package com.ml.commonutils;

/**
 * 返回状态码
 *
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/23 14:44
 */
public interface ResultCode {
    public static Integer SUCCESS = 200; // 成功
    public static Integer ERROR = 400; // 失败
}
