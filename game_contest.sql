/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.31 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

CREATE TABLE `ucenter_member` (
  `id` CHAR(19) NOT NULL COMMENT '用户id',
  `openid` VARCHAR(128) DEFAULT NULL COMMENT '微信openid',
  `mobile` VARCHAR(11) DEFAULT '' COMMENT '手机号',
  `password` VARCHAR(255) DEFAULT NULL COMMENT '密码',
  `nickname` VARCHAR(50) DEFAULT NULL COMMENT '昵称',
   `sex` TINYINT(2) UNSIGNED DEFAULT '1' COMMENT '性别 1 女，2 男',
  `age` TINYINT(3) UNSIGNED DEFAULT NULL COMMENT '年龄',
  `avatar` VARCHAR(255) DEFAULT NULL COMMENT '用户头像',
  `sign` VARCHAR(100) DEFAULT NULL COMMENT '用户签名',
  `is_parameter` VARCHAR(19) DEFAULT NULL COMMENT '参加哪场比赛的ID',
  `is_disabled` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '是否禁用 1（true）已禁用，  0（false）未禁用',
  `is_deleted` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` DATETIME NOT NULL COMMENT '创建时间',
  `gmt_modified` DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='用户表'

INSERT INTO `ucenter_member` (`id`, `openid`, `mobile`, `password`, `nickname`, `sex`, `age`, `avatar`, `sign`, `is_parameter`, `is_disabled`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1','vx123456','12345678910','96e79218965eb72c92a549dd5a330112','小爱同学','1','18','https://img2.woyaogexing.com/2020/10/22/e42ba052eee6499b90ca7f2d9810d215!400x400.webp','光芒四射','0','0','0','2020-10-26 03:56:38','2020-11-02 14:04:50');
INSERT INTO `ucenter_member` (`id`, `openid`, `mobile`, `password`, `nickname`, `sex`, `age`, `avatar`, `sign`, `is_parameter`, `is_disabled`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1320602170898374657','vx2222','18955555555','e10adc3949ba59abbe56e057f20f883e','小况同学','2','20',NULL,'万丈光芒','1321727911958429697','0','0','2020-10-26 13:44:28','2020-11-03 12:23:33');
INSERT INTO `ucenter_member` (`id`, `openid`, `mobile`, `password`, `nickname`, `sex`, `age`, `avatar`, `sign`, `is_parameter`, `is_disabled`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1322418233051000834','vx3333','18966666666','e10adc3949ba59abbe56e057f20f883e','上山打老虎', '1' ,NULL,'https://img2.woyaogexing.com/2020/10/22/e42ba052eee6499b90ca7f2d9810d215!400x400.webp',NULL,'1321727911958429697','0','0','2020-10-31 14:00:51','2020-11-03 12:24:02');


/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.31 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;

CREATE TABLE `sj_merchant` (
  `id` CHAR(19) NOT NULL COMMENT '供应商ID',
  `name` VARCHAR(20) NOT NULL COMMENT '供应商姓名',
  `intro` VARCHAR(500) NOT NULL DEFAULT '' COMMENT '供应商简介',
  `avatar` VARCHAR(255) DEFAULT NULL COMMENT '供应商logo',
  `sort` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT '排序',
  `is_deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` DATETIME NOT NULL COMMENT '创建时间',
  `gmt_modified` DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uk_name` (`name`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='供应商'

INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1','湖北KG集团有限责任公司','很牛逼','https://ml8.oss-cn-beijing.aliyuncs.com/2020/10/24/14d8db05e64f4d32a8b462c3ee5d7653file.png','0','0','2020-10-23 06:04:51','2020-10-24 15:45:06');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1319544319975223297','哈尔滨KG集团子公司','还可以','https://ml8.oss-cn-beijing.aliyuncs.com/2020/10/24/0746a7159a2440de8e29582828d9283dfile.png','0','0','2020-10-23 15:40:56','2020-10-24 15:58:06');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1319615348210728962','上海KG集团子公司','还可以','https://ml8.oss-cn-beijing.aliyuncs.com/2020/10/24/15c94379d59a4899bf3bb848d00d328ffile.png','0','0','2020-10-23 20:23:11','2020-10-23 20:23:11');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1319615381484142594','北京KG集团子公司','还可以','https://ml8.oss-cn-beijing.aliyuncs.com/2020/10/24/1c46ad32970044beaef94175d41f84a0file.png','0','0','2020-10-23 20:23:19','2020-10-23 20:23:19');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1319615422051450882','深圳KG集团子公司','还可以','https://ml8.oss-cn-beijing.aliyuncs.com/2020/10/24/14e34b7be64c4b5eaea474097a0b2da4file.png','0','0','2020-10-23 20:23:29','2020-10-24 15:45:21');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1319615435875876865','广州KG集团子公司','还可以','https://ml8.oss-cn-beijing.aliyuncs.com/2020/10/24/394eeb3acf0a4ff7987b3e5bdca76949file.png','0','0','2020-10-23 20:23:32','2020-10-23 20:23:32');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1319615462417432578','广东KG集团子公司','还可以','https://ml8.oss-cn-beijing.aliyuncs.com/2020/10/24/d6d3597bf6da4ec686449afe27015207file.png','0','0','2020-10-23 20:23:38','2020-10-24 15:45:33');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1319615499889344513','河南KG集团子公司','还可以','https://ml8.oss-cn-beijing.aliyuncs.com/2020/10/24/5dbb344427d143a589feb431b23b2849file.png','0','0','2020-10-23 20:23:47','2020-10-23 20:23:47');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1319615532621692929','新疆KG集团子公司','还可以','https://ml8.oss-cn-beijing.aliyuncs.com/2020/10/24/bae7296cb1f8438ea3412d796be15678file.png','0','0','2020-10-23 20:23:55','2020-10-24 15:46:02');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1319907156987846657','香港KG集团子公司','还可以','https://ml8.oss-cn-beijing.aliyuncs.com/2020/10/24/519c558835314827b5f6f38a173306dffile.png','0','0','2020-10-24 15:42:44','2020-10-24 15:58:45');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('2','湖北KG集团子公司','还可以','','0','0','2020-10-23 07:10:07','2020-10-23 07:10:09');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('3','湖南KG集团子公司','还可以','','0','0','2020-10-23 07:10:40','2020-10-23 07:10:41');
INSERT INTO `sj_merchant` (`id`, `name`, `intro`, `avatar`, `sort`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('4','湖北ML集团有限责任公司','很牛逼','','0','0','2020-10-23 07:13:10','2020-10-23 07:13:12');

/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.31 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;
# 商家比赛记录表
CREATE TABLE `contest_table` (
  `id` CHAR(19) NOT NULL COMMENT '比赛表id',
  `contest_id` VARCHAR(50) DEFAULT NULL COMMENT '竞赛游戏id',
  `contest_merchants` VARCHAR(100) DEFAULT NULL COMMENT '发起比赛商家',
  `avatar` VARCHAR(255) DEFAULT NULL COMMENT '商家头像',
  `start_time` DATETIME DEFAULT NULL COMMENT '比赛开始时间',
  `signup_total` INT(11) DEFAULT '0' COMMENT '报名人数',
  `is_deleted` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` DATETIME NOT NULL COMMENT '创建时间',
  `gmt_modified` DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='商家比赛记录表'

INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321727911958429697','1','深圳KG集团子公司',NULL,'2021-01-08 20:20:20','2','0','2020-10-29 16:17:45','2020-11-03 12:24:02');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321728170633740289','2','广州KG集团子公司',NULL,'2020-10-30 20:20:20','0','0','2020-10-29 16:18:47','2020-11-02 14:04:51');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321760861961850882','1','湖北KG集团有限责任公司',NULL,'2020-10-30 10:05:00','0','0','2020-10-29 18:28:41','2020-10-30 16:27:14');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321775171895304193','3','哈尔滨KG集团子公司',NULL,'2020-11-11 09:00:00','0','0','2020-10-29 19:25:33','2020-10-29 19:25:33');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321775195546984450','2','上海KG集团子公司',NULL,'2020-11-11 09:00:00','0','0','2020-10-29 19:25:39','2020-10-29 19:25:39');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321775210742947842','4','北京KG集团子公司',NULL,'2020-11-11 09:00:00','0','0','2020-10-29 19:25:42','2020-10-29 19:25:42');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321775226475782145','5','深圳KG集团子公司',NULL,'2020-11-11 09:00:00','0','0','2020-10-29 19:25:46','2020-10-29 19:25:46');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321775232926621697','5','广州KG集团子公司',NULL,'2020-11-11 09:00:00','0','0','2020-10-29 19:25:48','2020-10-29 19:25:48');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321775239532650498','5','广东KG集团子公司',NULL,'2020-11-11 09:00:00','0','0','2020-10-29 19:25:49','2020-10-29 19:25:49');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321775251620634625','5','河南KG集团子公司',NULL,'2020-11-11 09:00:00','0','0','2020-10-29 19:25:52','2020-10-29 19:25:52');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321775253524848642','5','河南KG集团子公司',NULL,'2020-11-11 09:00:00','0','0','2020-10-29 19:25:52','2020-10-29 19:25:52');
INSERT INTO `contest_table` (`id`, `contest_id`, `contest_merchants`, `avatar`, `start_time`, `signup_total`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1321775254321766402','5','河南KG集团子公司',NULL,'2020-11-11 09:00:00','0','0','2020-10-29 19:25:53','2020-10-29 19:25:53');

/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.31 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;
# 比赛游戏名称表
CREATE TABLE `game_name` (
  `id` CHAR(19) NOT NULL COMMENT 'id',
  `game_name` VARCHAR(50) DEFAULT NULL COMMENT '游戏名称',
  `is_deleted` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0' COMMENT '逻辑删除 1（true）已删除， 0（false）未删除',
  `gmt_create` DATETIME NOT NULL COMMENT '创建时间',
  `gmt_modified` DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='比赛游戏名称表'

INSERT INTO `game_name` (`id`, `game_name`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('1','英雄联盟','0','2020-11-02 01:43:19','2020-11-02 01:43:21');
INSERT INTO `game_name` (`id`, `game_name`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('2','王者荣耀','0','2020-11-02 01:43:58','2020-11-02 01:44:00');
INSERT INTO `game_name` (`id`, `game_name`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('3','绝地求生','0','2020-11-02 01:44:16','2020-11-02 01:44:18');
INSERT INTO `game_name` (`id`, `game_name`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('4','和平精英','0','2020-11-02 01:44:27','2020-11-02 01:44:29');
INSERT INTO `game_name` (`id`, `game_name`, `is_deleted`, `gmt_create`, `gmt_modified`) VALUES('5','第五人格','0','2020-11-02 01:44:36','2020-11-02 01:44:38');

/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.31 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;
# 用户个人比赛荣誉表
CREATE TABLE `ucenter_honor` (
  `id` CHAR(19) NOT NULL COMMENT 'id',
  `ucenter_id` CHAR(19) NOT NULL COMMENT '用户id',
  `champion` VARCHAR(10) DEFAULT '0' COMMENT  '冠军次数',
  `second` VARCHAR(10) DEFAULT '0' COMMENT  '亚军次数',
  `third` VARCHAR(10) DEFAULT '0' COMMENT  '季军次数',
  `contest_count` VARCHAR(10) DEFAULT '0' COMMENT  '参与比赛次数',
  `gmt_create` DATETIME NOT NULL COMMENT '创建时间',
  `gmt_modified` DATETIME NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='用户个人比赛荣誉表'

/*
SQLyog Community v13.1.6 (64 bit)
MySQL - 5.7.31 
*********************************************************************
*/
/*!40101 SET NAMES utf8 */;
# 评论区表
CREATE TABLE `contest_comment` (
  `id` CHAR(19) NOT NULL COMMENT 'id',
  `ucenterid` CHAR(19) DEFAULT NULL COMMENT '评论用户id',
  `avatar` VARCHAR(255) DEFAULT NULL COMMENT '评论用户头像',
  `content` VARCHAR(255) DEFAULT NULL COMMENT '评论内容',
  `good` VARCHAR(1) DEFAULT '0' COMMENT '是否点赞，(0) 未点赞， (1) 已点赞',
  `gmt_create` DATETIME NOT NULL COMMENT '创建时间',
  `gmt_modified` DATETIME NOT NULL COMMENT '更新时间',
   PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8mb4 COMMENT='评论区表'

INSERT INTO `contest_comment` (`id`, `ucenterid`, `avatar`, `content`, `good`, `gmt_create`, `gmt_modified`) VALUES('1323593672884764673','1','https://img2.woyaogexing.com/2020/10/22/e42ba052eee6499b90ca7f2d9810d215!400x400.webp','你好啊','0','2020-11-03 19:51:37','2020-11-03 19:51:37');
INSERT INTO `contest_comment` (`id`, `ucenterid`, `avatar`, `content`, `good`, `gmt_create`, `gmt_modified`) VALUES('1323593992406843393','1','https://img2.woyaogexing.com/2020/10/22/e42ba052eee6499b90ca7f2d9810d215!400x400.webp','你好啊','0','2020-11-03 19:52:54','2020-11-03 19:52:54');
INSERT INTO `contest_comment` (`id`, `ucenterid`, `avatar`, `content`, `good`, `gmt_create`, `gmt_modified`) VALUES('1323594111869009921','1','https://img2.woyaogexing.com/2020/10/22/e42ba052eee6499b90ca7f2d9810d215!400x400.webp','你好啊','0','2020-11-03 19:53:22','2020-11-03 19:53:22');
