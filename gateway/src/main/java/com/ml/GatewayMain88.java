package com.ml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/24 14:10
 */
@SpringBootApplication
@EnableDiscoveryClient
public class GatewayMain88 {
    public static void main(String[] args) {
        SpringApplication.run(GatewayMain88.class, args);
    }
}
