# GameContest
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://gitee.com/xiaokuang_nobug/game-contest/blob/master/LICENSE)
## 项目介绍
游戏比赛后台<br />
项目介绍：由各大厂商进行举办校园游戏比赛
单点登录模块，注册模块，用户报名模块，比赛开始之前通知模块，商家添加游戏比赛模块，等……


## 项目为微服务开发，主要使用SpringBoot框架，采用项目采用前后端分离架构<br>
- 采用Nacos作为注册中心、配置中心<br />
- 采用Gateway作为服务网关<br />
- 采用OpenFeign作为服务调用<br />
- 采用Hystrix作为服务降级熔断(敬请期待)<br />

## 项目结构
```
GameContest
├─ common -- 存放公共类
├─ doc -- 文档
├─ gateway -- 网关
├─ service -- 接口
|  ├─ service_comment -- 评论区接口
|  ├─ service_sms -- 阿里云msm短信服务接口
|  ├─ service_oss -- 阿里云OSS对象存储接口
|  ├─ service_sj -- 比赛供应商接口
|  ├─ service_ucenter -- 用户信息接口
```

## 软件架构

> 前端技术架构：Node.js + Vue.js + element-ui + Axios 等一系列热门技术 <br />
后端技术架构：springBoot + springCloud + Mybatis-Plus + Redis + Maven 等一系列热门技术 <br />
第三方：阿里云SMS + 阿里云OSS



## 安装教程
1.  启动Nacos（我用的windows本地的）
2.  启动Redis（我用的windows本地的）

## 使用说明
1.  启动前需要修改的配置文件[项目启动介绍](./doc/启动项目前需要修改的地方/初始化修改.md) <br />
2.  启动gateway网关模块 <br />
3.  启动service下所有的模块(如果不用阿里云短信登录和对象存储，可以不启动msm和oss模块) <br />
4.  启动前端项目 <br />


## 前端项目下载地址
[点击跳转至前端项目](https://gitee.com/xi_project/game-front-end)

