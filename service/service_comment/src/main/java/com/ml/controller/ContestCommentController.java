package com.ml.controller;


import com.ml.commonutils.Result;
import com.ml.entity.ContestComment;
import com.ml.service.ContestCommentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 评论区表 前端控制器
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-03
 */
@RestController
@RequestMapping("/comment")
@Api(tags = "评论区表")
public class ContestCommentController {

    @Autowired
    private ContestCommentService contestCommentService;

    @ApiOperation("添加一条评论")
    @PostMapping("/saveComment")
    public Result saveComment(@RequestBody ContestComment contestComment) {
        boolean result = contestCommentService.save(contestComment);
        if (result) return Result.ok();
        return Result.error();
    }

    @ApiOperation("查询全部评论")
    @GetMapping("/getCommentAll")
    public Result getComment() {
        List<ContestComment> list = contestCommentService.list(null);
        return Result.ok().data("items", list);
    }

}

