package com.ml.mapper;

import com.ml.entity.ContestComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 评论区表 Mapper 接口
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-03
 */
public interface ContestCommentMapper extends BaseMapper<ContestComment> {

}
