package com.ml.service;

import com.ml.entity.ContestComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 评论区表 服务类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-03
 */
public interface ContestCommentService extends IService<ContestComment> {

}
