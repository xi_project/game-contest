package com.ml.service.impl;

import com.ml.entity.ContestComment;
import com.ml.mapper.ContestCommentMapper;
import com.ml.service.ContestCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 评论区表 服务实现类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-03
 */
@Service
public class ContestCommentServiceImpl extends ServiceImpl<ContestCommentMapper, ContestComment> implements ContestCommentService {

}
