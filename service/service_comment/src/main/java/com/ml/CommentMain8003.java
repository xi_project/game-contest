package com.ml;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @作者 ShuHaoNan
 * @邮箱 shuhaonan1@gmail.com
 * @时间 2020/11/3 18:10
 */
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan(basePackages = "com.ml.mapper")
public class CommentMain8003 {
    public static void main(String[] args) {
        SpringApplication.run(CommentMain8003.class, args);
    }
}
