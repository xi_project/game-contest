package com.ml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/24 12:13
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
@EnableDiscoveryClient
public class OssMain8002 {
    public static void main(String[] args) {
        SpringApplication.run(OssMain8002.class, args);
    }
}
