package com.ml.controller;

import com.ml.commonutils.Result;
import com.ml.service.OssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/24 12:10
 */
@RestController
@RequestMapping("/fileoss")
@Api(tags = "阿里云OSS")
public class OssController {

    @Autowired
    private OssService ossService;

    /**
     * 上传供应商logo
     */
    @ApiOperation("上传供应商logo")
    @RequestMapping("/sjoss")
    public Result uploadOssFile(MultipartFile file) {
        String url = ossService.uploadFileAvatar(file);
        return Result.ok().data("url", url);
    }

}
