package com.ml.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/24 12:03
 */
public interface OssService {
    /**
     * 上传图片到OSS
     * @param file 流
     * @return
     */
    String uploadFileAvatar(MultipartFile file);
}
