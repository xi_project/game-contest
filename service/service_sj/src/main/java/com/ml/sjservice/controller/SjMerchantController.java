package com.ml.sjservice.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ml.commonutils.Result;
import com.ml.sjservice.entity.SjMerchant;
import com.ml.sjservice.service.SjMerchantService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * 供应商 前端控制器
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-23
 */
@RestController
@RequestMapping("/merchant")
@Api(tags = "供应商接口")
public class SjMerchantController {

    @Autowired
    private SjMerchantService sjMerchantService;

    /**
     * 添加供应商
     */
    @ApiOperation("添加供应商")
    @PostMapping("/save")
    public Result insertMerchant(@RequestBody SjMerchant sjMerchant) {

        boolean save = sjMerchantService.save(sjMerchant);
        if (save) {
            return Result.ok();
        }
        return Result.error();
    }

    /**
     * 修改供应商
     */
    @ApiOperation("修改供应商")
    @PutMapping("/update")
    public Result updateMerchant(@RequestBody SjMerchant sjMerchant) {
        boolean update = sjMerchantService.updateById(sjMerchant);
        if (update) {
        return Result.ok();
        }
        return Result.error();
    }

    /**
     * 逻辑删除供应商
     */
    @ApiOperation("逻辑删除供应商")
    @DeleteMapping("/delete/{id}")
    public Result removeMerchant(@PathVariable("id") String id) {
        if (sjMerchantService.removeById(id)) {
            return Result.ok();
        }
        return Result.error();
    }

    /**
     * 根据id查询供应商
     */
    @ApiOperation("根据id查询供应商")
    @GetMapping("/getMerchantById/{id}")
    public Result getMerchantById(@PathVariable("id") String id) {
        SjMerchant sjMerchant = sjMerchantService.getById(id);
        return Result.ok().data("merchant", sjMerchant);
    }

    /**
     * 查询所有供应商
     * @return 所有供应商
     */
    @ApiOperation("查询所有供应商")
    @GetMapping("/findAll")
    public Result findAllMerchant() {
        List<SjMerchant> list = sjMerchantService.list(null);
        return Result.ok().data("items", list);
    }

    /**
     * 分页查询供应商
     */
//    @ApiOperation("分页查询供应商")
//    @PostMapping("/pageMerchant/{current}/{limit}")
//    public Result pageListMerchant(@PathVariable("current") long current,
//                                   @PathVariable("limit") long limit) {
//        IPage<SjMerchant> page = sjMerchantService.pageListMerchant(current, limit);
//        // 总页数
//        long total = page.getTotal();
//        // 数据集合
//        List<SjMerchant> records = page.getRecords();
//        return Result.ok().data("total", total).data("rows",records);
//    }

    /**
     * 分页条件查询供应商
     */
    @ApiOperation("分页条件查询供应商")
    @PostMapping("/pageMerchantCondition/{current}/{limit}")
    public Result pageMerchantCondition(@PathVariable("current") long current,
                                   @PathVariable("limit") long limit,
                                   @RequestBody(required = false) SjMerchant sjMerchant) {
        IPage<SjMerchant> page = sjMerchantService.pageMerchantCondition(current, limit, sjMerchant);

        // 总页数
        long total = page.getTotal();
        // 数据集合
        List<SjMerchant> records = page.getRecords();
        return Result.ok().data("total", total).data("rows",records);
    }


}

