package com.ml.sjservice.service;

import com.ml.sjservice.entity.GameName;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 比赛游戏名称表 服务类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-02
 */
public interface GameNameService extends IService<GameName> {

}
