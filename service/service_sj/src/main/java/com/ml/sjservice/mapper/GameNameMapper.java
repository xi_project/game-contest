package com.ml.sjservice.mapper;

import com.ml.sjservice.entity.GameName;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 比赛游戏名称表 Mapper 接口
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-02
 */
public interface GameNameMapper extends BaseMapper<GameName> {

}
