package com.ml.sjservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ml.commonutils.Result;
import com.ml.sjservice.entity.GameName;
import com.ml.sjservice.service.GameNameService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 比赛游戏名称表 前端控制器
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-02
 */
@RestController
@RequestMapping("/sjservice/gamename")
@Api(tags = "比赛游戏名称表")
public class GameNameController {

    @Autowired
    private GameNameService gameNameService;

    /**
     * 查询所有比赛游戏名称
     *
     * @return 所有游戏名称
     */
    @ApiOperation("查询所有比赛游戏名称")
    @GetMapping("/getAll")
    public Result getGameNameAll() {
        List<GameName> gameNameList = gameNameService.list(null);
        return Result.ok().data("gamenameList", gameNameList);
    }



}

