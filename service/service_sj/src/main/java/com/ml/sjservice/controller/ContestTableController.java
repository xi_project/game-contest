package com.ml.sjservice.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ml.commonutils.Result;
import com.ml.sjservice.entity.ContestTable;
import com.ml.sjservice.entity.GameName;
import com.ml.sjservice.entity.vo.ContestTableStateVo;
import com.ml.sjservice.entity.vo.ContestTableVo;
import com.ml.sjservice.service.ContestTableService;
import com.ml.sjservice.service.GameNameService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 比赛记录表 前端控制器
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-29
 */
@RestController
@RequestMapping("/contestTable")
@Api(tags = "比赛记录表")
public class ContestTableController {

    @Autowired
    private ContestTableService contestTableService;

    @Autowired
    private GameNameService gameNameService;

    /**
     * 添加比赛
     *
     * @param contestTable 比赛信息
     * @return
     */
    @ApiOperation("添加比赛表")
    @PostMapping("/saveContestTable")
    public Result addContestTable(@RequestBody ContestTable contestTable) {
        boolean result = contestTableService.save(contestTable);
        if (result) return Result.ok();
        return Result.error().message("添加比赛表失败");
    }

    /**
     * 根据用户比赛ID，用户报名，报名人数 + 1
     *
     * @param id 当前比赛ID
     * @return
     */
    @ApiOperation("用户报名后，报名人数+1")
    @PutMapping("/updateContestTable/increase/{id}")
    public Result updateContestTableSigUpIncrease(@PathVariable("id") String id) {
        ContestTable tableServiceById = contestTableService.getById(id);
        ContestTable contestTable = new ContestTable();
        contestTable.setId(id);
        // 报名人数 == 原来人数 + 1
        int signTotal = tableServiceById.getSignupTotal() + 1;
        contestTable.setSignupTotal(signTotal);
        contestTableService.updateById(contestTable);
        return Result.ok();
    }

    /**
     * 根据用户比赛ID，用户报名，报名人数 - 1
     *
     * @param id 当前比赛ID
     * @return
     */
    @ApiOperation("用户报名后，报名人数-1")
    @PutMapping("/updateContestTable/reduce/{id}")
    public Result updateContestTableSigUpReduce(@PathVariable("id") String id) {
        ContestTable tableServiceById = contestTableService.getById(id);
        ContestTable contestTable = new ContestTable();
        contestTable.setId(id);
        // 报名人数 == 原来人数 - 1
        int signTotal = tableServiceById.getSignupTotal() + -1;
        contestTable.setSignupTotal(signTotal);
        contestTableService.updateById(contestTable);
        return Result.ok();
    }

    /**
     * 根据ID逻辑删除比赛表
     *
     * @param id 比赛表ID
     * @return
     */
    @ApiOperation("根据ID逻辑删除比赛表")
    @DeleteMapping("/deleteContestTable/{id}")
    public Result deleteContestTableById(@PathVariable("id") String id) {
        boolean result = contestTableService.removeById(id);
        if (result) return Result.ok();
        return Result.error().message("删除比赛表失败");
    }

    /**
     * 根据比赛名称查询比赛
     *
     * @param name 比赛名称
     * @return
     */
    @ApiOperation("根据比赛名称查询比赛")
    @GetMapping("/getContestTableByName/{contestName}")
    public Result getContestTableByName(@PathVariable("contestName") String name) {
        List<ContestTable> contestTableList = contestTableService.selectContestTableByName(name);
        return Result.ok().data("contestTableList", contestTableList);
    }

    /**
     * 查询全部比赛
     *
     * @return
     */
    @ApiOperation("查询全部比赛")
    @GetMapping("/getContestTable/All")
    public Result getContestTableAll() {
        List<ContestTable> contestTableList = contestTableService.list(null);
        return Result.ok().data("items", contestTableList);
    }

    /**
     * 条件查询带分页
     *
     * @param current 当前页
     * @param limit 分页
     * @param name 比赛名称
     * @return
     */
    @ApiOperation("分页查询比赛")
    @GetMapping("/getContestTablePage/{current}/{limit}")
    public Result getContestTablePage(@PathVariable("current") long current,
                                      @PathVariable("limit") long limit,
                                      @RequestParam(value = "name", required = false) String name) {

        IPage<ContestTable> contestTableIPage = contestTableService.selectPageContestTableCondition(current, limit, name);
        // 总页数
        long total = contestTableIPage.getTotal();
        // 比赛表数据
        List<ContestTable> contestTableList = contestTableIPage.getRecords();

        // 封装对象contestTableVo对象
        List<ContestTableVo> contestTableVsList = contestTableService.encapsulationVo(contestTableList);

        return Result.ok().data("total", total).data("rows", contestTableVsList);
    }

    /**
     * 条件查询带分页，新增个状态标签用于比赛列表展示
     *
     * @param current 当前页
     * @param limit 分页
     * @param name 比赛名称
     * @return
     */
    @ApiOperation("分页查询比赛新增个状态标签用于比赛列表展示")
    @GetMapping("/getContestTablePageState/{current}/{limit}")
    public Result getContestTablePageState(@PathVariable("current") long current,
                                      @PathVariable("limit") long limit,
                                      @RequestParam(value = "name", required = false) String name) {

        IPage<ContestTable> contestTableIPage = contestTableService.selectPageContestTableCondition(current, limit, name);
        // 总页数
        long total = contestTableIPage.getTotal();
        // 比赛表数据
        List<ContestTable> contestTableList = contestTableIPage.getRecords();

        // 封装对象contestTableVo对象
        List<ContestTableStateVo> contestTableVsList = contestTableService.encapsulationStateVo(contestTableList);

        return Result.ok().data("total", total).data("rows", contestTableVsList);
    }


}


























