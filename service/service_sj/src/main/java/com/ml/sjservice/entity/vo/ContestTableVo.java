package com.ml.sjservice.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 *
 * @作者 ShuHaoNan
 * @邮箱 shuhaonan1@gmail.com
 * @时间 2020/11/2 11:38
 */
@Data
public class ContestTableVo {

    @ApiModelProperty(value = "比赛表id")
    private String id;

    @ApiModelProperty(value = "竞赛游戏名称")
    private String contestName;

    @ApiModelProperty(value = "发起比赛商家")
    private String contestMerchants;

    @ApiModelProperty(value = "商家头像")
    private String avatar;

    @ApiModelProperty(value = "比赛开始时间")
    private Date startTime;

    @ApiModelProperty(value = "报名人数")
    private int signupTotal;

    @ApiModelProperty(value = "逻辑删除 1（true）已删除， 0（false）未删除")
    private Boolean isDeleted;

    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @ApiModelProperty(value = "更新时间")
    private Date gmtModified;

}
