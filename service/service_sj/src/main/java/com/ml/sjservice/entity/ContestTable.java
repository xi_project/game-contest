package com.ml.sjservice.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 比赛记录表
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ContestTable对象", description="比赛记录表")
public class ContestTable implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "比赛表id")
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    @ApiModelProperty(value = "竞赛游戏id")
    private String contestId;

    @ApiModelProperty(value = "发起比赛商家")
    private String contestMerchants;

    @ApiModelProperty(value = "商家头像")
    private String avatar;

    @ApiModelProperty(value = "比赛开始时间")
    private Date startTime;

    @ApiModelProperty(value = "报名人数")
    private int signupTotal;

    @ApiModelProperty(value = "逻辑删除 1（true）已删除， 0（false）未删除")
    private Boolean isDeleted;

    @TableField(fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date gmtCreate;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    @ApiModelProperty(value = "更新时间")
    private Date gmtModified;


}
