package com.ml.sjservice.controller;

import com.ml.commonutils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

/**
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/23 17:40
 */
@RestController
@RequestMapping("/sjservice/user")
@Api(tags = "管理员后台登录")
public class LoginController {

    /**
     * 登录
     */
    @ApiOperation("登录")
    @PostMapping("/login")
    public Result lgoin() {

        return Result.ok().data("token", "admin");
    }

    @ApiOperation("信息")
    @GetMapping("/info")
    public Result info() {
        return Result.ok().data("roles", "[admin]").data("name", "admin").data("avatar", "https://img2.woyaogexing.com/2020/10/22/e42ba052eee6499b90ca7f2d9810d215!400x400.webp");
    }

}
