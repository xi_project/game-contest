package com.ml.sjservice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ml.sjservice.entity.ContestTable;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ml.sjservice.entity.vo.ContestTableStateVo;
import com.ml.sjservice.entity.vo.ContestTableVo;

import java.util.List;

/**
 * <p>
 * 比赛记录表 服务类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-29
 */
public interface ContestTableService extends IService<ContestTable> {

    /**
     * 根据比赛名称查询比赛
     *
     * @param name
     * @return
     */
    List<ContestTable> selectContestTableByName(String name);

    /**
     * 条件查询带分页
     *
     * @param current 当前页
     * @param limit 分页
     * @param name 比赛名称
     * @return
     */
    IPage<ContestTable> selectPageContestTableCondition(long current, long limit, String name);

    /**
     * 封装对象contestTableVo对象
     *
     * @param contestTableList
     * @return
     */
    List<ContestTableVo> encapsulationVo(List<ContestTable> contestTableList);

    /**
     * 封装对象contestTableStateVo对象
     *
     * @param contestTableList
     * @return
     */
    List<ContestTableStateVo> encapsulationStateVo(List<ContestTable> contestTableList);
}
