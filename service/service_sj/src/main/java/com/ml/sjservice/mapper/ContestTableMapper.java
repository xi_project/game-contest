package com.ml.sjservice.mapper;

import com.ml.sjservice.entity.ContestTable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 比赛记录表 Mapper 接口
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-29
 */
public interface ContestTableMapper extends BaseMapper<ContestTable> {

}
