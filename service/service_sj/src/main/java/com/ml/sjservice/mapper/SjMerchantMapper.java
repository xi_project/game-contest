package com.ml.sjservice.mapper;

import com.ml.sjservice.entity.SjMerchant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 供应商 Mapper 接口
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-23
 */
public interface SjMerchantMapper extends BaseMapper<SjMerchant> {

}
