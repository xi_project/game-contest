package com.ml.sjservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ml.sjservice.entity.ContestTable;
import com.ml.sjservice.entity.GameName;
import com.ml.sjservice.entity.vo.ContestTableStateVo;
import com.ml.sjservice.entity.vo.ContestTableVo;
import com.ml.sjservice.mapper.ContestTableMapper;
import com.ml.sjservice.service.ContestTableService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ml.sjservice.service.GameNameService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import sun.rmi.runtime.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>
 * 比赛记录表 服务实现类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-29
 */
@Service
public class ContestTableServiceImpl extends ServiceImpl<ContestTableMapper, ContestTable> implements ContestTableService {

    @Autowired
    private GameNameService gameNameService;

    /**
     * 根据比赛名称查询比赛
     *
     * @param name
     * @return
     */
    @Override
    public List<ContestTable> selectContestTableByName(String name) {
        QueryWrapper<ContestTable> wrapper = new QueryWrapper<>();
        wrapper.eq("contest_name", name);
        return baseMapper.selectList(wrapper);
    }

    /**
     * 条件查询带分页
     *
     * @param current 当前页
     * @param limit 分页
     * @param name 比赛名称
     * @return
     */
    @Override
    public IPage<ContestTable> selectPageContestTableCondition(long current, long limit, String name) {
        // 分页条件 name 是 游戏名字 ， 而数据库中是id ，重写SQL
        Page<ContestTable> page = new Page<>(current, limit);
        QueryWrapper<ContestTable> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(name)) {
            // 根据游戏名称查询id
            QueryWrapper<GameName> gameNameQueryWrapper = new QueryWrapper<>();
            gameNameQueryWrapper.eq("game_name", name);
            GameName gameName = (GameName) gameNameService.getObj(gameNameQueryWrapper);
            String gameNameId = gameName.getId();
            // 定义根据游戏ID查询
            wrapper.eq("contest_id", gameNameId);
        }
        return baseMapper.selectPage(page, wrapper);
    }

    /**
     * 封装对象contestTableVo对象发送给服务器渲染前端页面
     *
     * @param contestTableList
     * @return
     */
    @Override
    public List<ContestTableVo> encapsulationVo(List<ContestTable> contestTableList) {
        // 所有游戏名称
        Map<String, String> map = getGameName();

        List<ContestTableVo> contestTableVsList = new ArrayList<>();

        for (ContestTable contestTable : contestTableList) {
            ContestTableVo contestTableVo = new ContestTableVo();
            BeanUtils.copyProperties(contestTable, contestTableVo);
            // 获取游戏ID，从put中取值
            String contestId = contestTable.getContestId();
            contestTableVo.setContestName(map.get(contestId));
            contestTableVsList.add(contestTableVo);
        }
        return contestTableVsList;
    }

    /**
     * 封装对象contestTableVo对象，新增状态属性，发送给服务器渲染前端页面
     *
     * @param contestTableList
     * @return
     */
    @Override
    public List<ContestTableStateVo> encapsulationStateVo(List<ContestTable> contestTableList) {
        // 所有游戏名称
        Map<String, String> map = getGameName();
        List<ContestTableStateVo> contestTableStateVoList = new ArrayList<>();

        for (ContestTable contestTable : contestTableList) {
            ContestTableStateVo contestTableStateVo = new ContestTableStateVo();
            BeanUtils.copyProperties(contestTable, contestTableStateVo);
            // 获取游戏ID，从put中取值
            String contestId = contestTable.getContestId();
            contestTableStateVo.setContestName(map.get(contestId));

            // 比赛时间算一个小时 加一个小时时间，1000 * 60 * 60 才算结束
            Date date1 = new Date();
            Date date = new Date(date1.getTime() + (1000 * 60 * 60));
            if (date.before(contestTableStateVo.getStartTime())) {
                contestTableStateVo.setContestState("未开始");
            } else if (date.after(contestTableStateVo.getStartTime())) {
                contestTableStateVo.setContestState("已结束");
            } else {
                contestTableStateVo.setContestState("进行中");
            }
            contestTableStateVoList.add(contestTableStateVo);
        }
        return contestTableStateVoList;
    }

    /**
     * 查询所有游戏名称，key：id值， value：游戏名称
     *
     * @return
     */
    private Map<String, String> getGameName() {
        // 查询所有游戏名称
        QueryWrapper<GameName> wrapper = new QueryWrapper<>();
        wrapper.select("id" ,"game_name");
        List<GameName> gameNameList = gameNameService.list(wrapper);

        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < gameNameList.size(); i++) {
            String id = gameNameList.get(i).getId();
            String gameName = gameNameList.get(i).getGameName();
            // key：id值， value：游戏名称
            map.put(id, gameName);
        }
        return map;
    }

}






















