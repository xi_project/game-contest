package com.ml.sjservice.service.impl;

import com.ml.sjservice.entity.GameName;
import com.ml.sjservice.mapper.GameNameMapper;
import com.ml.sjservice.service.GameNameService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 比赛游戏名称表 服务实现类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-02
 */
@Service
public class GameNameServiceImpl extends ServiceImpl<GameNameMapper, GameName> implements GameNameService {

}
