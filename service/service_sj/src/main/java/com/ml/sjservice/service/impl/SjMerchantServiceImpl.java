package com.ml.sjservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ml.sjservice.entity.SjMerchant;
import com.ml.sjservice.mapper.SjMerchantMapper;
import com.ml.sjservice.service.SjMerchantService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * <p>
 * 供应商 服务实现类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-23
 */
@Service
public class SjMerchantServiceImpl extends ServiceImpl<SjMerchantMapper, SjMerchant> implements SjMerchantService {

    /**
     * 分页查询供应商
     */
//    @Override
//    public IPage<SjMerchant> pageListMerchant(long current, long limit) {
//        // 创建page对象
//        Page<SjMerchant> page = new Page<>(current, limit);
//        return baseMapper.selectPage(page, null);
//    }

    /**
     * 分页条件查询供应商
     */
    @Override
    public IPage<SjMerchant> pageMerchantCondition(long current, long limit, SjMerchant sjMerchant) {
        // 创建page对象
        Page<SjMerchant> pageMerchant = new Page<>(current, limit);
        // 构造条件
        QueryWrapper<SjMerchant> wrapper = new QueryWrapper<>();
        if (!StringUtils.isEmpty(sjMerchant.getName())) {
            wrapper.like("name", sjMerchant.getName());
        }

        return baseMapper.selectPage(pageMerchant, wrapper);
    }



}
