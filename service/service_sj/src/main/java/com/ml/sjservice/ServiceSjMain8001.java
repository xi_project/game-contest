package com.ml.sjservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/23 14:07
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.ml")
@EnableDiscoveryClient
public class ServiceSjMain8001 {
    public static void main(String[] args) {
        SpringApplication.run(ServiceSjMain8001.class, args);
    }
}
