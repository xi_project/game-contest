package com.ml.sjservice.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.ml.sjservice.entity.SjMerchant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 供应商 服务类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-23
 */
public interface SjMerchantService extends IService<SjMerchant> {

    /**
     * 分页查询供应商
     */
//    IPage<SjMerchant> pageListMerchant(long current, long limit);

    /**
     * 分页条件查询供应商
     */
    IPage<SjMerchant> pageMerchantCondition(long current, long limit, SjMerchant sjMerchant);

}
