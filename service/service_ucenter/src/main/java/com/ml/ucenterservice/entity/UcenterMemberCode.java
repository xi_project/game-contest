package com.ml.ucenterservice.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 验证码登录
 *
 * @作者 ShuHaoNan
 * @邮箱 shuhaonan1@gmail.com
 * @时间 2020/10/31 14:06
 */
@Data
@ApiModel(value="UcenterMemberCode", description="用户表验证码登录")
public class UcenterMemberCode implements Serializable {

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "验证码")
    private String code;

}
