package com.ml.ucenterservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ml.commonutils.JwtUtils;
import com.ml.commonutils.MD5;
import com.ml.commonutils.RandomUtil;
import com.ml.servicebase.handler.exceptionhandler.CustomException;
import com.ml.ucenterservice.constant.RedisConstant;
import com.ml.ucenterservice.entity.UcenterMember;
import com.ml.ucenterservice.entity.UcenterMemberCode;
import com.ml.ucenterservice.mapper.UcenterMemberMapper;
import com.ml.ucenterservice.service.UcenterMemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.concurrent.TimeUnit;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-26
 */
@Service
public class UcenterMemberServiceImpl extends ServiceImpl<UcenterMemberMapper, UcenterMember> implements UcenterMemberService {

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 密码登录
     *
     * @param ucenterMember 用户
     * @return redisToken
     */
    @Override
    public String login(UcenterMember ucenterMember) {
        // 获取登录手机号和密码
        String mobile = ucenterMember.getMobile();
        String password = ucenterMember.getPassword();
        // 手机号和密码非空判断
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)) throw new CustomException("登录失败");
        // 判断手机号是否正确
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        UcenterMember mobileMember = baseMapper.selectOne(wrapper);
        // 判断查出来的对象是否为空
        if (mobileMember == null) throw new CustomException("登录失败");
        // 判断MD5加密后的密码是否正确
        if (!MD5.encrypt(password).equals(mobileMember.getPassword())) throw new CustomException("登录失败");
        return setRedisKey(mobileMember);
    }

    /**
     * 验证码登录
     * @param ucenterMemberCode 用户
     * @return redisToken
     */
    @Override
    public String codeLogin(UcenterMemberCode ucenterMemberCode) {
        // 获取登录手机号和密码
        String mobile = ucenterMemberCode.getMobile();
        String code = ucenterMemberCode.getCode();
        // 手机号和验证码非空判断
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(code)) throw new CustomException("登录失败");
        // 判断手机号是否正确
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        UcenterMember mobileMember = baseMapper.selectOne(wrapper);
        // 判断查出来的对象是否为空
        if (mobileMember == null) throw new CustomException("登录失败");
        // 判断验证码是否正确
        String redisCode = (String) redisTemplate.opsForValue().get(mobile);
        if (!code.equals(redisCode)) throw new CustomException("登录失败");
        return setRedisKey(mobileMember);
    }

    /**
     * 注册
     *
     * @param ucenterMember 用户信息
     */
    @Override
    public void register(UcenterMember ucenterMember) {
        // 手机号
        String mobile = ucenterMember.getMobile();
        // 昵称
        String nickname = ucenterMember.getNickname();
        // 密码
        String password = ucenterMember.getPassword();
        // 判断是否为空
        if (StringUtils.isEmpty(mobile) || StringUtils.isEmpty(password)
                || StringUtils.isEmpty(nickname)) throw new CustomException("注册失败");
        // 从Redis中取出验证码，判断验证码是否正确
        String redisCode = (String) redisTemplate.opsForValue().get(mobile);
        // 判断手机号是否重复
        QueryWrapper<UcenterMember> wrapper = new QueryWrapper<>();
        wrapper.eq("mobile", mobile);
        if (baseMapper.selectCount(wrapper) > 0) throw new CustomException("注册失败");
        // 密码进行MD5加密后添加到数据中
        UcenterMember member = ucenterMember.setPassword(MD5.encrypt(password));
        baseMapper.insert(member);
    }

    /**
     * 把用户信息存入Redis中
     *
     * @param ucenterMember 用户
     * @return
     */
    private String setRedisKey(UcenterMember ucenterMember) {
        // 判断用户是否被禁用
        if (ucenterMember.getIsDisabled()) throw new CustomException("登录失败");
        // 生成token值
        String token = JwtUtils.getJwtToken(ucenterMember.getId(), ucenterMember.getNickname());
        // 登录成功 把token放入Redis中，key为 user:手机号:info，value为token
        String key = RedisConstant.TOKEN_KEY_USER + ucenterMember.getMobile() + RedisConstant.TOKEN_KEY_INFO;
        // 设置token过期时间为一天
        redisTemplate.opsForValue().set(key, token, 1, TimeUnit.DAYS);
        // 登录成功 返回Redis中随机值
        return key;
    }

}
















