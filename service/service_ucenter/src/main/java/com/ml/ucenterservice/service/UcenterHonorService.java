package com.ml.ucenterservice.service;

import com.ml.ucenterservice.entity.UcenterHonor;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 用户个人比赛荣誉表 服务类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-02
 */
public interface UcenterHonorService extends IService<UcenterHonor> {

}
