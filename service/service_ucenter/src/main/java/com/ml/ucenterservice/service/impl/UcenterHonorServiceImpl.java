package com.ml.ucenterservice.service.impl;

import com.ml.ucenterservice.entity.UcenterHonor;
import com.ml.ucenterservice.mapper.UcenterHonorMapper;
import com.ml.ucenterservice.service.UcenterHonorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户个人比赛荣誉表 服务实现类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-02
 */
@Service
public class UcenterHonorServiceImpl extends ServiceImpl<UcenterHonorMapper, UcenterHonor> implements UcenterHonorService {

}
