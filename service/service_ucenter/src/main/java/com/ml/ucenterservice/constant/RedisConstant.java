package com.ml.ucenterservice.constant;

/**
 * redis存放token的key的值
 *
 * @作者 ShuHaoNan
 * @邮箱 shuhaonan1@gmail.com
 * @时间 2020/10/29 11:32
 */
public class RedisConstant {

    public static final String TOKEN_KEY_USER = "user:";
    public static final String TOKEN_KEY_INFO = ":info";

}
