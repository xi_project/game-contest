package com.ml.ucenterservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/26 11:32
 */
@SpringBootApplication
@ComponentScan("com.ml")
@MapperScan("com.ml.ucenterservice.mapper")
@EnableFeignClients(basePackages = "com.ml.ucenterservice.feign")
public class UCenterMain8004 {
    public static void main(String[] args) {
        SpringApplication.run(UCenterMain8004.class, args);
    }
}
