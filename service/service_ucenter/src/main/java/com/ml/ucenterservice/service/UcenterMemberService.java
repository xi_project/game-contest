package com.ml.ucenterservice.service;

import com.ml.ucenterservice.entity.UcenterMember;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ml.ucenterservice.entity.UcenterMemberCode;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-26
 */
public interface UcenterMemberService extends IService<UcenterMember> {

    /**
     * 密码登录
     * @param ucenterMember 用户
     * @return token
     */
    String login(UcenterMember ucenterMember);

    /**
     * 验证码登录
     * @param ucenterMemberCode 用户
     * @return
     */
    String codeLogin(UcenterMemberCode ucenterMemberCode);

    /**
     * 注册
     * @param ucenterMember 用户信息
     */
    void register(UcenterMember ucenterMember);

}
