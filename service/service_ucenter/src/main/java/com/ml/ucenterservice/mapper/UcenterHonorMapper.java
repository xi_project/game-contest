package com.ml.ucenterservice.mapper;

import com.ml.ucenterservice.entity.UcenterHonor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户个人比赛荣誉表 Mapper 接口
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-02
 */
public interface UcenterHonorMapper extends BaseMapper<UcenterHonor> {

}
