package com.ml.ucenterservice.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ml.commonutils.Result;
import com.ml.ucenterservice.entity.UcenterHonor;
import com.ml.ucenterservice.service.UcenterHonorService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户个人比赛荣誉表 前端控制器
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-11-02
 */
@RestController
@RequestMapping("/ucenter")
@Api(tags = "用户个人比赛荣誉表")
public class UcenterHonorController {

    @Autowired
    private UcenterHonorService ucenterHonorService;

    /**
     * 新增用户荣誉墙
     *
     * @param id 用户ID
     * @return
     */
    @ApiOperation("新增用户荣誉")
    @PostMapping("/saveUcenterHonor/{id}")
    public Result saveUcenterHonor(@PathVariable("id") String id) {
        UcenterHonor ucenterHonor = new UcenterHonor();
        ucenterHonor.setUcenterId(id);
        boolean result = ucenterHonorService.save(ucenterHonor);
        if (result) return Result.ok();
        return Result.error();
    }

    /**
     * 修改用户信息
     *
     * @param ucenterHonor 用户荣誉信息
     * @return
     */
    @ApiOperation("修改用户荣誉信息")
    @PutMapping("/updateUcenterHonor")
    public Result updateUcenterHonor(@RequestBody UcenterHonor ucenterHonor) {
        boolean result = ucenterHonorService.updateById(ucenterHonor);
        if (result) return Result.ok();
        return Result.error();
    }

    /**
     * 根据用户id查询用户荣誉信息
     *
     * @param id 用户ID
     * @return
     */
    @ApiOperation("根据用户id查询用户荣誉信息")
    @GetMapping("/selectUcenterHonor/{id}")
    public Result selectUcenterHonor(@PathVariable("id") String id) {
        QueryWrapper<UcenterHonor> wrapper = new QueryWrapper<>();
        wrapper.eq("ucenter_id", id);
        int count = ucenterHonorService.count(wrapper);
        // 如果荣誉表还没创建，则先创建
        if (count <= 0) {
            saveUcenterHonor(id);
        }

        QueryWrapper<UcenterHonor> wrapperUcenterHonor = new QueryWrapper<>();
        wrapper.eq("ucenter_id", id);
        UcenterHonor ucenterHonor = ucenterHonorService.getOne(wrapperUcenterHonor);
        return Result.ok().data("ucenterHonor", ucenterHonor);
    }



}

