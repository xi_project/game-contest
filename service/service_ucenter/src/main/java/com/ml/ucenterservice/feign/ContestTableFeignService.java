package com.ml.ucenterservice.feign;

import com.ml.commonutils.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;

/**
 * Openfeign远程调用比赛表
 *
 * @作者 ShuHaoNan
 * @邮箱 shuhaonan1@gmail.com
 * @时间 2020/10/30 15:30
 */
@FeignClient("service-sj")
public interface ContestTableFeignService {

    /**
     * 根据用户比赛ID，用户报名，报名人数 + 1
     */
    @PutMapping("/contestTable/updateContestTable/increase/{id}")
    public Result updateContestTableSigUpIncrease(@PathVariable("id") String id);

    /**
     * 根据用户比赛ID，用户报名，报名人数 - 1
     */
    @PutMapping("/contestTable/updateContestTable/reduce/{id}")
    public Result updateContestTableSigUpReduce(@PathVariable("id") String id);

}
