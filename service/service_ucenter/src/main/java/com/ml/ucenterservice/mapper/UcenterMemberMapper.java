package com.ml.ucenterservice.mapper;

import com.ml.ucenterservice.entity.UcenterMember;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author ShuHaoNan
 * @since 2020-10-26
 */
public interface UcenterMemberMapper extends BaseMapper<UcenterMember> {

}
