package com.ml.service;

import java.util.Map;

/**
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/26 12:40
 */
public interface MsmService {
    /**
     * 发送短信
     */
    boolean send(Map<String, Object> param, String phone);

    /**
     * 获取验证码
     * @param phone 手机号
     */
    String getCode(String phone);
}
