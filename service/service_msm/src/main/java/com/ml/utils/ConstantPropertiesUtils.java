package com.ml.utils;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 初始化常量
 *
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/24 11:58
 */
@Component
public class ConstantPropertiesUtils implements InitializingBean {

    // 读取配置文件
    @Value("${aliyun.msm.file.keyid}")
    private String keyId;
    @Value("${aliyun.msm.file.keysecret}")
    private String keySecret;
    @Value("${aliyun.msm.file.signName}")
    private String signName;
    @Value("${aliyun.msm.file.templateCode}")
    private String TemplateCode;

    public static String KEY_ID;
    public static String KEY_SECRET;
    public static String SIGN_NAME;
    public static String TEMPLATE_CODE;

    @Override
    public void afterPropertiesSet() throws Exception {
        KEY_ID = keyId;
        KEY_SECRET = keySecret;
        SIGN_NAME = signName;
        TEMPLATE_CODE = TemplateCode;
    }
}
