package com.ml;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author ShuHaoNan
 * @email shuhaonan1@gmail.com
 * @date 2020/10/26 12:36
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class MsmMain8005 {
    public static void main(String[] args) {
        SpringApplication.run(MsmMain8005.class, args);
    }
}
